/**
 * Initialize the Leap Controller on the Drupal Website
 * Set the init options for the library
 */

LeapManager.init({

    enableMetaGestures: true,
    enableDefaultMetaGestureActions: true,
    maxCursors:2,
    enableHoverTap: true,
    enablePressDown: true,
    enableTouchScrolling: false,
    enableScrollbarScrolling: true,

});


/**
 * Add the specific LeapStrap attributes to the Body and the anchors
 */
(function ($) {
    Drupal.behaviors.leap_motion = {
        attach: function (context, settings) {

           $('body').addClass('leap-interactive').attr('leap-disable-tap', 'true').attr('leap-disable-hover', 'true');

        }
    };
})(jQuery);